#/bin/bash

format_output ()
{
	while IFS= read -r line
	do
		now=$(date +"%m-%d-%Y_%r")
		echo "$now: $1 $line"
	done	
}

if [ -z $1 ]; then
	echo "usage: ./bootstrap <list of IPs>"
else
	for i in "$@"; do
		now=$(date +"%m-%d-%Y_%r")
		echo "$now Bootstrapping node $i"
		sleep 5
		ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no "root@$i" 'bash -xs' < script 2>&1 | format_output $i
	done
fi