#
# This is an example VCL file for Varnish.
#
# It does not do anything by default, delegating control to the
# builtin VCL. The builtin VCL is called when there is no explicit
# return statement.
#
# See the VCL chapters in the Users Guide at https://www.varnish-cache.org/docs/
# and http://varnish-cache.org/trac/wiki/VCLExamples for more examples.

# Marker to tell the VCL compiler that this VCL has been adapted to the
# new 4.0 format.
vcl 4.0;

# Default backend definition. Set this to point to your content server.
backend default {
  .host = "127.0.0.1";
  .port = "8000";
}

backend api_social {
  .host = "127.0.0.1";
  .port = "8002";
}

backend social_admin {
  .host = "127.0.0.1";
  .port = "8003";
}

backend api_cbk {
  .host = "127.0.0.1";
  .port = "8004";
}

backend api_cfl {
  .host = "127.0.0.1";
  .port = "8005";
}

backend api_mlb {
  .host = "127.0.0.1";
  .port = "8006";
}

backend api_nba {
  .host = "127.0.0.1";
  .port = "8007";
}

backend api_nfl {
  .host = "127.0.0.1";
  .port = "8008";
}

backend api_nhl {
  .host = "127.0.0.1";
  .port = "8009";
}

backend api_voting {
  .host = "127.0.0.1";
  .port = "8010";
}

sub vcl_recv {
  # Happens before we check if we have this in cache already.
  #
  # Typically you clean up the request here, removing cookies you don't need,
  # rewriting the request, etc.

  # Unset the cookie in the request header unless this request is for the social admin tool.
  if ( req.http.host !~ "social\-admin.onetwosee" ) {
    unset req.http.Cookie;
  }

  if ( req.http.host ~ "api.social.onetwosee" ) {
    set req.backend_hint = api_social;
  } else if ( req.http.host ~ "social\-admin.onetwosee"  ) {
    set req.backend_hint = social_admin;
  } else if ( req.http.host ~ "api.cbk" ) {
    set req.backend_hint = api_cbk;
  } else if ( req.http.host ~ "api.cfl" ) {
    set req.backend_hint = api_cfl;
  } else if ( req.http.host ~ "api.mlb" ) {
    set req.backend_hint = api_mlb;
  } else if ( req.http.host ~ "api.nba" ) {
    set req.backend_hint = api_nba;
  } else if ( req.http.host ~ "api.nfl" ) {
    set req.backend_hint = api_nfl;
  } else if ( req.http.host ~ "api.nhl.onetwosee" ) {
    unset req.http.Access-Control-Request-Headers;
    set req.backend_hint = api_nhl;
  } else if ( req.http.host ~ "api.onetwosee" ) {
    unset req.http.Access-Control-Request-Headers;
    set req.backend_hint = api_nhl;
  } else if ( req.http.host ~ "api.voting" ) {
    set req.backend_hint = api_voting;
    return(pass);
  } else {
    set req.backend_hint = default;
  }
}

sub vcl_backend_response {
  # Happens after we have read the response headers from the backend.
  #
  # Here you clean the response headers, removing silly Set-Cookie headers
  # and other mistakes your backend does.

  # Responses from the voting api should not be cached no matter what.
  if ( bereq.http.host ~ "api.voting" ) {
    if ( beresp.http.Cache-Control ~ "(no-cache|private)" || beresp.http.pragma ~ "no-cache" ) {
      set beresp.ttl = 0s;
    }
  }

  # Let the social admin set the Set-Cookie header. We do not need to cache responses from this backend.
  if ( bereq.http.host !~ "social\-admin.onetwosee" ) {
    unset beresp.http.Set-Cookie;
  }

}

sub vcl_deliver {
  # Happens when we have all the pieces we need, and are about to send the
  # response to the client.
  #
  # You can do accounting or modifying the final object here.

  # Explicitly set Access-Control-Allow-Origin to * for all responses.
  unset resp.http.Access-Control-Allow-Origin;
  set resp.http.Access-Control-Allow-Origin = req.http.origin;
  set resp.http.Access-Control-Allow-Credentials = "true";

  return (deliver);
}

